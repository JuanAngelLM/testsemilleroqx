<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Tarjeta de identidad            Cedu_fb5d4e</name>
   <tag></tag>
   <elementGuidId>dece33ef-24cc-4677-b4db-eec46ef03158</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='typeDocument']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#typeDocument</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>235614a2-a9fd-459e-8068-f0e1374df4be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>typeDocument</value>
      <webElementGuid>84966980-408c-4787-a71d-c7696dce19f0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            Tarjeta de identidad
            Cedula de ciudadania
          </value>
      <webElementGuid>dc36385c-cb87-466a-91f1-819263094ab3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;typeDocument&quot;)</value>
      <webElementGuid>019e7d63-06ad-44b2-bd85-8421e467732c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='typeDocument']</value>
      <webElementGuid>49d7e367-2499-4d04-992e-a0c7bfad64ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='formulario']/div/div/select</value>
      <webElementGuid>b236b187-a492-495d-ba18-e11ccbadcd01</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tipo Documento'])[1]/following::select[1]</value>
      <webElementGuid>da949d95-d5fa-43d1-94a8-5e2cfb6d8ca7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[4]/following::select[1]</value>
      <webElementGuid>94e6da4f-2832-48e3-9c6a-27cffe3515a1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[5]/preceding::select[1]</value>
      <webElementGuid>7ff75ae6-160a-4b15-a8cd-7892024b8fe8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
      <webElementGuid>40cca1ff-b388-42d0-ac54-143f6ed52d7b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@id = 'typeDocument' and (text() = '
            Tarjeta de identidad
            Cedula de ciudadania
          ' or . = '
            Tarjeta de identidad
            Cedula de ciudadania
          ')]</value>
      <webElementGuid>d16ad35f-095d-42e9-9e84-04cd324a02d3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
