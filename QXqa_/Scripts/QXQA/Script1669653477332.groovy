package org.apache.commons.lang3;
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.util.Random

def random(n) {
	letras = (('A'..'Z')).join()
	randomString = RandomStringUtils.random(n, letras.toCharArray())
	return randomString
}

def getTenDigitsNumber() {
	TenDigits = 1000000000 + Math.random() * 9000000000;
	return (String) Math.round(TenDigits);
}

def getOneDigitNumber() {
	OneDigit = 1 + Math.random() * 9;
	return (String) Math.round(OneDigit);
}

WebUI.openBrowser('')

WebUI.navigateToUrl('https://jovial-syrniki-01d3ea.netlify.app/')

WebUI.click(findTestObject('Object Repository/Page_Aslie/a_Registrate aqu'))

WebUI.setText(findTestObject('Object Repository/Page_Registrate/input__Email'), random(5) + '@yopmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Registrate/input_Contrasena_passw'), 'cDbzSVqoOR4ZSDO9XOFwfA==')

WebUI.setText(findTestObject('Object Repository/Page_Registrate/input__name'), random(6))

WebUI.setText(findTestObject('Object Repository/Page_Registrate/input__grado'), getOneDigitNumber())

WebUI.setText(findTestObject('Object Repository/Page_Registrate/input__grupo'), getOneDigitNumber())

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Registrate/select_Tarjeta de identidad            Cedu_fb5d4e'), 
    'Cedula', true)

WebUI.setText(findTestObject('Object Repository/Page_Registrate/input__document'), getTenDigitsNumber())

WebUI.click(findTestObject('Object Repository/Page_Registrate/button_Continuar'))

WebUI.setText(findTestObject('Object Repository/Page_Document/input_Nmero documento_task-nombre'), getTenDigitsNumber())

WebUI.setText(findTestObject('Object Repository/Page_Document/input_Grado y Grupo_task-grado'), getOneDigitNumber() + '°' + getOneDigitNumber())

WebUI.setText(findTestObject('Object Repository/Page_Document/input_Nombre_task-implemento'), random(5) + " " + random(5))

WebUI.click(findTestObject('Object Repository/Page_Document/button_Guardar'))
